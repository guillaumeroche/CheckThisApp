package fr.guillaumeroche.checkthisapp;

import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class ApplicationManager {
	
	private static ApplicationManager mInstance = new ApplicationManager();
	private ThreadPoolExecutor mThreadPoolExecutor;
	
	
	private ApplicationManager() {
		mThreadPoolExecutor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
	}
	
	static public void startLoad(ExtendedApplicationInfo appInfo) {
		mInstance.mThreadPoolExecutor.execute(appInfo);
	}

}
