package fr.guillaumeroche.checkthisapp.fragment;

import java.util.ArrayList;

import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.nfc.NfcAdapter.CreateBeamUrisCallback;
import android.nfc.NfcEvent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.text.format.Formatter;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView.MultiChoiceModeListener;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ShareActionProvider;
import android.widget.ShareActionProvider.OnShareTargetSelectedListener;
import fr.guillaumeroche.checkthisapp.ExtendedApplicationInfo;
import fr.guillaumeroche.checkthisapp.MyApplication;
import fr.guillaumeroche.checkthisapp.R;
import fr.guillaumeroche.checkthisapp.activity.AppsActivity;
import fr.guillaumeroche.checkthisapp.enums.ApplicationType;

public class AppListFragment extends ListFragment {

    private static final String ARG_TYPE = "type";

    private ActionMode mActionMode;
    private ShareActionProvider mShareActionProvider;
    private Intent mShareIntent;
    private ApplicationType mType;
    private ArrayList<Uri> fileList;

    public static AppListFragment create(ApplicationType type) {
        AppListFragment fragment = new AppListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_TYPE, type.ordinal());
        fragment.setArguments(args);
        return fragment;
    }

    public AppListFragment() {
        mActionMode = null;

        // Create a share Intent
        mShareIntent = new Intent(Intent.ACTION_SEND_MULTIPLE);
        mShareIntent.setType("application/vnd.android.package-archive");

        fileList = new ArrayList<Uri>();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mType = ApplicationType.values()[getArguments().getInt(ARG_TYPE)];
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.application_list, container, false);

        return rootView;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void setupNfc(AppsActivity activity) {

        NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(activity);
        if (nfcAdapter != null) {
            nfcAdapter.setBeamPushUrisCallback(new CreateBeamUrisCallback() {

                @Override
                public Uri[] createBeamUris(NfcEvent event) {
                    return (Uri[]) fileList.toArray();
                }
            }, activity);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        AppsActivity activity = (AppsActivity) getActivity();

        switch (mType) {
        case USER:
            setListAdapter(activity.getUserAppsListAdapter());
            break;
        case SYSTEM:
            setListAdapter(activity.getSystemAppsListAdapter());
            break;
        case SHARED:
            setListAdapter(activity.getSharedAppsListAdapter());
            break;
        default:
            break;
        }

        // Create the NFC callback
        setupNfc(activity);

        final ListView listView = getListView();

        // Enable selection on short item click
        listView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                // Invert the selection status of the clicked item
                listView.setItemChecked(position, !listView.isItemChecked(position));
            }
        });

        // Enable multiple selection on the list view
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);

        // Set the selection listener
        listView.setMultiChoiceModeListener(new MultiChoiceModeListener() {

            @Override
            public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
                mActionMode = mode;
                return false;
            }

            @Override
            public void onDestroyActionMode(ActionMode mode) {
                mActionMode = null;
            }

            @Override
            public boolean onCreateActionMode(ActionMode mode, Menu menu) {
                // Inflate context menu
                MenuInflater inflater = mode.getMenuInflater();
                inflater.inflate(R.menu.context, menu);

                mShareActionProvider = (ShareActionProvider) menu.findItem(R.id.action_share).getActionProvider();
                mShareActionProvider.setShareIntent(mShareIntent);

                // Register a listener
                mShareActionProvider.setOnShareTargetSelectedListener(new OnShareTargetSelectedListener() {

                    @Override
                    public boolean onShareTargetSelected(ShareActionProvider source, Intent intent) {

                        SparseBooleanArray selectedItems = getListView().getCheckedItemPositions();
                        Adapter adapter = getListAdapter();
                        ExtendedApplicationInfo appInfo;

                        for (int i = 0; i < getListAdapter().getCount(); i++) {
                            if (selectedItems.get(i)) {
                                appInfo = (ExtendedApplicationInfo) adapter.getItem(i);

                                if (appInfo.getShareCount() == 0) {
                                    // Add the shared app to the list
                                    MyApplication.getInstance().getSharedApps().add(appInfo);
                                }

                                // Increment the share count
                                appInfo.setShareCount(appInfo.getShareCount() + 1);
                            }
                        }

                        // Close CAB
                        mActionMode.finish();

                        // Update UI
                        ((AppsActivity) getActivity()).getSharedAppsListAdapter().notifyDataSetChanged();

                        // The return result is ignored. Always return false for
                        // consistency
                        return false;
                    }
                });

                return true;
            }

            @Override
            public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {

                // Remove existing extras from the share intent
                mShareIntent.replaceExtras((Bundle) null);

                // Total byte size
                long totalByteSize = 0;

                // Browse the selected items
                Adapter adapter = getListAdapter();
                ExtendedApplicationInfo appInfo;

                fileList.clear();

                SparseBooleanArray selectedItems = getListView().getCheckedItemPositions();
                for (int i = 0; i < adapter.getCount(); i++) {
                    if (selectedItems.get(i)) {
                        appInfo = (ExtendedApplicationInfo) adapter.getItem(i);
                        fileList.add(Uri.fromFile(appInfo.getApplicationFile()));
                        totalByteSize += appInfo.getApplicationFile().length();
                    }
                }

                // Update the share intent
                mShareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, fileList);
                mShareActionProvider.setShareIntent(mShareIntent);

                // Update title and subtitle
                mode.setTitle(String.valueOf(selectedItems.size()) + " Selected");
                mode.setSubtitle(Formatter.formatFileSize(getActivity(), totalByteSize));
            }
        });
    }

    public void finishActionMode() {
        if (mActionMode != null) {
            mActionMode.finish();
        }
    }

}
