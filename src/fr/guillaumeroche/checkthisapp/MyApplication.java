package fr.guillaumeroche.checkthisapp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;

public class MyApplication extends Application {
	
	public static final String PREFS_NAME = "settings";
	
	// Static reference to the application
	private static MyApplication sInstance = null;
	
	// Loading flag
	private boolean mLoading = false;
	
	// Data lists
	private List<ExtendedApplicationInfo> mSystemApps = new ArrayList<ExtendedApplicationInfo>();
	private List<ExtendedApplicationInfo> mUserApps = new ArrayList<ExtendedApplicationInfo>();
	private List<ExtendedApplicationInfo> mSharedApps = new ArrayList<ExtendedApplicationInfo>();
	
	@Override
	public void onCreate() {
		super.onCreate();
		
		//  Set the instance static variable
		sInstance = this;
		
		// Start to load the lists
		loadData();
	}
	
	public void loadData() {
		updateTaskStatus(true);
		
		// Define anonymous inner AsyncTask
		AsyncTask<Void, Void, Void> sortAppsTask = new AsyncTask<Void, Void, Void>() {
			
			private List<ExtendedApplicationInfo> systemApps = new ArrayList<ExtendedApplicationInfo>();
			private List<ExtendedApplicationInfo> userApps = new ArrayList<ExtendedApplicationInfo>();
			private List<ExtendedApplicationInfo> sharedApps = new ArrayList<ExtendedApplicationInfo>();

			@Override
			protected Void doInBackground(Void... params) {
				
				// Get shared preferences
				SharedPreferences settings = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);
				
				// Get package list
				List<ApplicationInfo> packages = getPackageManager().getInstalledApplications(PackageManager.GET_META_DATA);

				// Browse packages
				Iterator<ApplicationInfo> it = packages.iterator();
				while (it.hasNext()) {
					// Separate the apps in categories
					ExtendedApplicationInfo currentApp = new ExtendedApplicationInfo(it.next(), getPackageManager());
					
					// Set the share count
					currentApp.setShareCount(settings.getInt(currentApp.packageName, 0));
					
					if ((currentApp.flags & ApplicationInfo.FLAG_SYSTEM) == ApplicationInfo.FLAG_SYSTEM) {
						systemApps.add(currentApp);
					} else {
						userApps.add(currentApp);
					}
					
					if (currentApp.getShareCount() > 0) {
						sharedApps.add(currentApp);
					}
				}
				
				// Sort collections
				Collections.sort(systemApps);
				Collections.sort(userApps);
				
				// Use specific comparator to show most shared apps first
				Collections.sort(sharedApps, new Comparator<ExtendedApplicationInfo> () {

					@Override
					public int compare(ExtendedApplicationInfo app0, ExtendedApplicationInfo app1) {
						return app1.getShareCount() - app0.getShareCount();
					}
					
				});
				
				return null;
			}

			@Override
			protected void onPostExecute(Void arg) {
				updateTaskStatus(false);
				
				// Clear member lists
				mSystemApps.clear();
				mUserApps.clear();
				mSharedApps.clear();
				
				// Add local lists to member lists
				mSystemApps.addAll(systemApps);
				mUserApps.addAll(userApps);
				mSharedApps.addAll(sharedApps);
			}

			@Override
			protected void onCancelled() {
				super.onCancelled();
				
				updateTaskStatus(false);
			}

		};
		
		sortAppsTask.execute();
	}
	
	private void updateTaskStatus(boolean loading) {
		mLoading = loading;
		
		// Create intent and send it
		Intent intent = new Intent("MyApp.Task");
		sendBroadcast(intent);
	}

	public static MyApplication getInstance() {
		return sInstance;
	}

	public boolean isLoading() {
		return mLoading;
	}

	public List<ExtendedApplicationInfo> getSystemApps() {
		return mSystemApps;
	}

	public List<ExtendedApplicationInfo> getUserApps() {
		return mUserApps;
	}
	
	public List<ExtendedApplicationInfo> getSharedApps() {
		return mSharedApps;
	}

}
