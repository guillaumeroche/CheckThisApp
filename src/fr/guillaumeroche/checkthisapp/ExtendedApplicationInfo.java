package fr.guillaumeroche.checkthisapp;

import java.io.File;

import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Process;

public class ExtendedApplicationInfo extends ApplicationInfo implements Comparable<ExtendedApplicationInfo>, Runnable {
	
	private final PackageManager mPackageManager;
	private File mApplicationFile;
	private Drawable mApplicationIcon;
	private int mShareCount;
	
	public ExtendedApplicationInfo(ApplicationInfo applicationInfo, final PackageManager packageManager)	{
		super(applicationInfo);
		mPackageManager = packageManager;
		name = loadLabel(packageManager).toString();
		mApplicationFile = new File(sourceDir);
		// The icon load will be done in another thread as it is a time consuming operation
		mApplicationIcon = loadIcon(mPackageManager);
	}

	public File getApplicationFile() {
		return mApplicationFile;
	}

	public Drawable getApplicationIcon() {
		return mApplicationIcon;
	}

	public int getShareCount() {
		return mShareCount;
	}

	public void setShareCount(int shareCount) {
		mShareCount = shareCount;
	}

	@Override
	public void run() {
		// Set the thread priority to background
		Process.setThreadPriority(Process.THREAD_PRIORITY_BACKGROUND);
		
		// Load the icon
		mApplicationIcon = loadIcon(mPackageManager);
	}

	@Override
	public int compareTo(ExtendedApplicationInfo arg0) {
		return name.compareToIgnoreCase(arg0.name);
	}

}
