package fr.guillaumeroche.checkthisapp.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.nfc.NfcEvent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.ListFragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnActionExpandListener;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import fr.guillaumeroche.checkthisapp.ExtendedApplicationInfo;
import fr.guillaumeroche.checkthisapp.ListAdapter;
import fr.guillaumeroche.checkthisapp.MyApplication;
import fr.guillaumeroche.checkthisapp.R;
import fr.guillaumeroche.checkthisapp.enums.ApplicationType;
import fr.guillaumeroche.checkthisapp.fragment.AppListFragment;

public class AppsActivity extends FragmentActivity implements ActionBar.TabListener {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	private PagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	private ViewPager mViewPager;
	
	private ListAdapter systemAppsListAdapter;
	private ListAdapter userAppsListAdapter;
	private ListAdapter sharedAppsListAdapter;
	
	private BroadcastReceiver mBroadcastReceiver;
	private MenuItem mRefreshMenuItem;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_apps);

		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}
		});
		
		if (savedInstanceState != null) {
			// TODO restore selected tab : mViewPager.setCurrentItem(savedInstanceState.getInt("pageItem"));
		}

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i))
					.setTabListener(this));
		};
		
		systemAppsListAdapter = new ListAdapter(ApplicationType.SYSTEM, this);
		userAppsListAdapter = new ListAdapter(ApplicationType.USER, this);
		sharedAppsListAdapter = new ListAdapter(ApplicationType.SHARED, this);
		
		// Create the broadcast receiver
		mBroadcastReceiver = new BroadcastReceiver() {

			@Override
			public void onReceive(Context context, Intent intent) {
				updateUi();
			}
			
		};

	}
	
	private void updateUi() {
		
		if (MyApplication.getInstance().isLoading()) {
			// Display progress bar in the menu
			if (mRefreshMenuItem != null) {
				mRefreshMenuItem.setActionView(R.layout.progress_bar);
			}
		} else {
			// Display refresh icon in the menu
			if (mRefreshMenuItem != null) {
				mRefreshMenuItem.setActionView(null);
			}
			// Update list adapters
			systemAppsListAdapter.notifyDataSetChanged();
			userAppsListAdapter.notifyDataSetChanged();
			sharedAppsListAdapter.notifyDataSetChanged();
		}
	}

	public ListAdapter getSystemAppsListAdapter() {
		return systemAppsListAdapter;
	}

	public ListAdapter getUserAppsListAdapter() {
		return userAppsListAdapter;
	}
	
	public ListAdapter getSharedAppsListAdapter() {
		return sharedAppsListAdapter;
	}

	@Override
	protected void onPause() {
		super.onPause();
		
		unregisterReceiver(mBroadcastReceiver);
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		registerReceiver(mBroadcastReceiver, new IntentFilter("MyApp.Task"));
		updateUi();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		
		// Save the apps share count
		saveShareCount(MyApplication.getInstance().getUserApps());
		saveShareCount(MyApplication.getInstance().getSystemApps());
	}
	
	private void saveShareCount(List<ExtendedApplicationInfo> appList) {
		// Get the shared preferences editor
		SharedPreferences.Editor editor = getSharedPreferences(MyApplication.PREFS_NAME, Context.MODE_PRIVATE).edit();
		
		Iterator<ExtendedApplicationInfo> it = appList.iterator();
		while (it.hasNext()) {
			ExtendedApplicationInfo currentApp = it.next();
			
			if (currentApp.getShareCount() > 0) {
				// Store the share count in shared preferences
				editor.putInt(currentApp.packageName, currentApp.getShareCount());
			}
		}
		
		// Save the files shared, to mark them as 'favorites'
		editor.commit();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt("pageItem", mViewPager.getCurrentItem());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main_menu, menu);
		
		// Store the reference of the refresh menu item
		mRefreshMenuItem = menu.findItem(R.id.action_refresh);
		
		// Set a listener to handle the collapse of the menu item
		MenuItem searchMenuItem = menu.findItem(R.id.action_search);
		searchMenuItem.setOnActionExpandListener(new OnActionExpandListener() {
			
			@Override
			public boolean onMenuItemActionExpand(MenuItem item) {
				return true;
			}
			
			@Override
			public boolean onMenuItemActionCollapse(MenuItem item) {
				filterApps("");
				return true;
			}
		});
		
		// Associate the searchable configuration with the search view
		SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
		SearchView searchView = (SearchView) searchMenuItem.getActionView();
		searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
		
		searchView.setOnQueryTextListener(new OnQueryTextListener() {
			@Override
			public boolean onQueryTextSubmit(String query) {
				return false;
			}
			
			@Override
			public boolean onQueryTextChange(String newText) {
				filterApps(newText);
				return true;
			}
		});
		
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_refresh:
			MyApplication.getInstance().loadData();
			break;
		case R.id.action_select_all:
			//AppListFragment currentFragment = (AppListFragment) getFragmentManager().getFragments().get(mViewPager.getCurrentItem());
			ListView currentListView = getFragment(mViewPager.getCurrentItem()).getListView();
			for (int i = 0; i < currentListView.getCount(); i++) {
				currentListView.setItemChecked(i, true);
			}
				
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}
	
	@Override
	public void onTabReselected(Tab tab, FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction fragmentTransaction) {
		// Tab is unselected, we need to dismiss the contextual action bar
		getFragment(tab.getPosition()).finishActionMode();
	}
	
	@Override
	protected void onNewIntent(Intent intent) {

		if (intent.getAction().compareTo(Intent.ACTION_SEARCH) == 0) {
			String query = intent.getStringExtra(SearchManager.QUERY);
			filterApps(query);
		}
		else {
			super.onNewIntent(intent);
		}
	}
	
	@SuppressWarnings("unchecked")
	private void filterApps(final String name) {
		if (name.isEmpty()) {
			systemAppsListAdapter.setApplicationList(MyApplication.getInstance().getSystemApps());
			userAppsListAdapter.setApplicationList(MyApplication.getInstance().getUserApps());
			sharedAppsListAdapter.setApplicationList(MyApplication.getInstance().getSharedApps());
			return;
		}
		
		// Define anonymous inner AsyncTask
		AsyncTask<List<ExtendedApplicationInfo>, Void, Void> filterAppsTask;
		filterAppsTask = new AsyncTask<List<ExtendedApplicationInfo>, Void, Void>() {
			
			private List<ExtendedApplicationInfo> systemApps = new ArrayList<ExtendedApplicationInfo>();
			private List<ExtendedApplicationInfo> userApps = new ArrayList<ExtendedApplicationInfo>();
			private List<ExtendedApplicationInfo> sharedApps = new ArrayList<ExtendedApplicationInfo>();
			Locale locale = Locale.getDefault();

			@Override
			protected Void doInBackground(List<ExtendedApplicationInfo>... params) {
				int count = params.length;
				for (int i = 0; i < count; i++) {
					// Browse packages
					Iterator<ExtendedApplicationInfo> it = params[i].iterator();
					while (it.hasNext()) {
						// Separate the apps in categories
						ExtendedApplicationInfo currentApp = it.next();
						if (currentApp.name.toLowerCase(locale).contains(name.toLowerCase(locale))) {
							if ((currentApp.flags & ApplicationInfo.FLAG_SYSTEM) == ApplicationInfo.FLAG_SYSTEM) {
								systemApps.add(currentApp);
							} else {
								userApps.add(currentApp);
							}
							
							if (currentApp.getShareCount() > 0) {
								sharedApps.add(currentApp);
							}
						}
					}
				}
				
				// Sort filtered lists
				Collections.sort(systemApps);
				Collections.sort(userApps);
				Collections.sort(sharedApps);
				
				return null;
			}

			@Override
			protected void onPostExecute(Void arg) {
				// Set lists to adapters
				systemAppsListAdapter.setApplicationList(systemApps);
				userAppsListAdapter.setApplicationList(userApps);
				sharedAppsListAdapter.setApplicationList(sharedApps);
			}

		};
		
		filterAppsTask.execute(MyApplication.getInstance().getSystemApps(),
				MyApplication.getInstance().getUserApps());
	}

	private AppListFragment getFragment(int position) {
		return (AppListFragment) getSupportFragmentManager().getFragments().get(position);
	}
	
	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fragmentManager) {
			super(fragmentManager);
		}

		@Override
		public Fragment getItem(int position) {
			// getItem is called to instantiate the fragment for the given page.
			// Return a DummySectionFragment (defined as a static inner class
			// below) with the page number as its lone argument.
			ListFragment fragment = null;
			switch (position) {
			case 0:
				fragment = AppListFragment.create(ApplicationType.USER);
				break;
			case 1:
				fragment = AppListFragment.create(ApplicationType.SYSTEM);
				break;
			case 2:
				fragment = AppListFragment.create(ApplicationType.SHARED);
				break;
			}

			return fragment;
		}

		@Override
		public int getCount() {
			/*if (MyApplication.getInstance().getSharedApps().isEmpty()) {
				return 2;
			}*/
			return 3;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section_user).toUpperCase(l);
			case 1:
				return getString(R.string.title_section_system).toUpperCase(l);
			case 2:
				return getString(R.string.title_section_shared).toUpperCase(l);
			default:
				return null;
			}
		}
		
		
	}

}
