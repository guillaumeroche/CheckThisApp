package fr.guillaumeroche.checkthisapp.activity;

import java.util.ArrayList;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.nfc.NfcAdapter;
import android.os.Build;
import android.os.Bundle;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
public class NfcShareActivity extends Activity {
	
	private NfcAdapter mNfcAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		Intent intent = getIntent();
		String action = intent.getAction();
		String type = intent.getType();
				
		if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_NFC)) {
			Toast.makeText(this, "No NFC on your device", Toast.LENGTH_SHORT).show();
			return;
		}
		
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
			Toast.makeText(this, "NFC not supported", Toast.LENGTH_SHORT).show();
			return;
		}
		
		mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
		
		if (Intent.ACTION_SEND_MULTIPLE.equals(action) && 
			type != null &&
			type.equals("application/vnd.android.package-archive")) {
			
			handleSendApps(intent);
		}
	}
	
	private void handleSendApps(Intent intent) {
		ArrayList<Uri> packageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
		if (mNfcAdapter != null) {
			mNfcAdapter.setBeamPushUris(packageUris.toArray(new Uri[packageUris.size()]), this);
		}
	}
	

}
