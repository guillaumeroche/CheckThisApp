package fr.guillaumeroche.checkthisapp;

import java.util.List;

import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.graphics.Color;
import android.text.format.Formatter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import fr.guillaumeroche.checkthisapp.R;

import fr.guillaumeroche.checkthisapp.enums.ApplicationType;

public class ListAdapter extends BaseAdapter {
	
	private List<ExtendedApplicationInfo> mApplicationList;
	private Activity mContext;
	
	public ListAdapter(ApplicationType applicationType, Activity context)
	{
		switch (applicationType) {
		case SYSTEM:
			mApplicationList = MyApplication.getInstance().getSystemApps();
			break;
		case USER:
			mApplicationList = MyApplication.getInstance().getUserApps();
			break;
		case SHARED:
			mApplicationList = MyApplication.getInstance().getSharedApps();
			break;
		default:
			mApplicationList = null;
			break;
		}
		
		mContext = context;
	}

	public void setApplicationList(List<ExtendedApplicationInfo> applicationList) {
		mApplicationList = applicationList;
		notifyDataSetChanged();
	}

	@Override
	public int getCount() {
		if (mApplicationList == null)
		{
			return 0;
		}
		return mApplicationList.size();
	}

	@Override
	public Object getItem(int position) {
		if (mApplicationList == null || position >= getCount()) {
			return null;
		}
		return mApplicationList.get(position);
	}

	@Override
	public long getItemId(int position) {
		if (mApplicationList == null || position >= getCount()) {
			return 0;
		}
		return position;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		if (view == null)
		{
			// instantiate a new view from item layout
			LayoutInflater inflater = mContext.getLayoutInflater();
			view = inflater.inflate(R.layout.list_item, null);
		}
		
		if (mApplicationList == null)
		{
			return view;
		}
		
		TextView title = (TextView) view.findViewById(R.id.textPackageName);
		TextView size = (TextView) view.findViewById(R.id.textSize);
		ImageView icon = (ImageView) view.findViewById(R.id.icon);
		
		ExtendedApplicationInfo appInfo = mApplicationList.get(position); 
		
		title.setText(appInfo.name);
		size.setText(Formatter.formatFileSize(mContext, appInfo.getApplicationFile().length()));
		if (appInfo.getApplicationIcon() == null) {
			ApplicationManager.startLoad(appInfo);
		}
		else {
			icon.setImageDrawable(appInfo.getApplicationIcon());
		}
		
		if ((appInfo.flags & ApplicationInfo.FLAG_SYSTEM) == ApplicationInfo.FLAG_SYSTEM) {
			title.setTextColor(Color.RED);
		}
		else {
			title.setTextColor(Color.BLUE);
		}
		
		return view;
	}

}
