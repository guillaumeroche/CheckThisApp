package fr.guillaumeroche.checkthisapp.enums;

public enum ApplicationType {
	SYSTEM,
	USER,
	SHARED
}
